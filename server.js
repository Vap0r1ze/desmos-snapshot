require('dotenv').config()
const _ = require('lodash')
const crypto = require('crypto')
const db = require('quick.db')
const express = require('express')
const filesize = require('filesize')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan')
const sharp = require('sharp')
const shortid = require('shortid')
const desmosSnapshot = require('./desmosSnapshot')
const app = express()

const patterns = {
  number: /^\d+$/,
  decimal: /^[-+]?\d*\.?\d+/,
  boolean: /^true|false$/,
  grid: /^false|cartesian|polar$/,
  arrows: /^false|both|positive$/,
}

app.use(morgan((tokens, req, res) => [
  tokens.method(req, res),
  tokens.url(req, res),
  tokens.status(req, res),
  tokens.res(req, res, 'x-age'), '-',
  tokens.res(req, res, 'x-size'), '/',
  tokens['response-time'](req, res) + 'ms',
].join(' ')))

app.get('/snapshot/:id', async (req, res) => {
  const {
    grid,
    arrows,
    axisNumbers,
    minorGridLines,
    xAxis,
    yAxis,
    xLabel,
    yLabel,
    x,
    y,
    width,
    height,
    pwidth,
    pheight,
    zoom,
  } = req.query
  const { id } = req.params
  const options = {}
  const postOptions = {}
  if (patterns.grid.test(grid)) options.grid = grid
  if (patterns.arrows.test(arrows)) options.arrows = arrows
  if (patterns.number.test(width)) options.width = _.clamp(+width, 100, 5000)
  if (patterns.number.test(height)) options.height = _.clamp(+height, 100, 5000)
  if (patterns.number.test(pwidth)) postOptions.width = _.clamp(+pwidth, 100, 5000)
  if (patterns.number.test(pheight)) postOptions.height = _.clamp(+pheight, 100, 5000)
  if (patterns.number.test(zoom)) options.zoom = _.clamp(+zoom, -10, 10)
  if (patterns.boolean.test(axisNumbers)) options.axisNumbers = axisNumbers
  if (patterns.boolean.test(minorGridLines)) options.minorGridLines = minorGridLines
  if (patterns.boolean.test(xAxis)) options.xAxis = xAxis
  if (patterns.boolean.test(yAxis)) options.yAxis = yAxis
  if (patterns.decimal.test(x) && Number.isSafeInteger(Math.floor(+x))) options.x = +x
  if (patterns.decimal.test(y) && Number.isSafeInteger(Math.floor(+y))) options.y = +y
  if (typeof xLabel === 'string') options.xLabel = xLabel
  if (typeof yLabel === 'string') options.yLabel = yLabel
  for (const [k, v] of Object.entries(options)) {
    if (patterns.boolean.test(v)) options[k] = JSON.parse(v)
  }
  const optionsHash = crypto.createHash('sha256').update(id + JSON.stringify(options)).digest('hex')
  let imgId = db.get(`img:${optionsHash}`)
  if (!fs.existsSync(path.join(__dirname, 'snapshots')))
    fs.mkdirSync(path.join(__dirname, 'snapshots'))
  let snapshot
  if (imgId && fs.existsSync(path.join(__dirname, `snapshots/${imgId}.png`))) {
    snapshot = fs.readFileSync(path.join(__dirname, `snapshots/${imgId}.png`))
    res.set('x-age', 'old')
  } else {
    console.log('generating')
    snapshot = await desmosSnapshot(id, options)
    imgId = shortid()
    db.set(`img:${optionsHash}`, imgId)
    fs.writeFileSync(path.join(__dirname, `snapshots/${imgId}.png`), snapshot)
    console.log(`generated ${imgId}`)
    res.set('x-age', 'new')
  }
  if (postOptions.width || postOptions.height) {
    const img = sharp(snapshot)
    const { width: w, height: h } = await img.metadata()
    postOptions.width = _.clamp(postOptions.width || w, w)
    postOptions.height = _.clamp(postOptions.height || h, h)
    snapshot = await img.extract({
      left: Math.floor((w - postOptions.width) / 2),
      top: Math.floor((h - postOptions.height) / 2),
      width: postOptions.width,
      height: postOptions.height,
    }).toBuffer()
  }
  res.set('x-size', filesize(snapshot.length).replace(' ', ''))
  res.type('png').send(snapshot)
})

app.listen(process.env.PORT)
