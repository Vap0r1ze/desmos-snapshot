const chromium = require('chromium')
const puppeteer = require('puppeteer')

function sleep (ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

const needEditOpts = [ 'grid', 'arrows', 'axisNumbers', 'minorGridLines', 'xAxis', 'yAxis', 'xLabel', 'yLabel', 'x', 'y' ]

async function setInput (page, input, text) {
  await input.click()
  await page.keyboard.down('Control')
  await page.keyboard.press('A')
  await page.keyboard.up('Control')
  await page.keyboard.press('Backspace')
  await input.type(text)
}

async function desmosSnapshot (graphId, options) {
  const browser = await puppeteer.launch({
    executablePath: chromium.path,
    headless: true,
  })
  const page = await browser.newPage()
  let dispGraphId = graphId
  if (needEditOpts.some(opt => opt in options)) {
    await page.goto(`https://www.desmos.com/calculator/${graphId}`)
    await page.waitForSelector('.dcg-action-settings')
    await page.click('.dcg-action-settings')
    await page.waitForSelector('.dcg-settings-container')

    // Get old options
    const oldOptions = await page.evaluate(() => {
      const settings = document.querySelector('.dcg-settings-container')
      const oldOptions = {}
      oldOptions.grid = settings.querySelectorAll('.dcg-grid-settings .dcg-component-checkbox')[0].classList.contains('dcg-checked')
      oldOptions.arrows = settings.querySelectorAll('.dcg-grid-settings .dcg-component-checkbox')[1].classList.contains('dcg-checked')
      oldOptions.axisNumbers = settings.querySelector('.dcg-top-checkboxes-container .dcg-component-checkbox:nth-child(1)').classList.contains('dcg-checked')
      oldOptions.xAxis = settings.querySelector('.dcg-x-axis-title .dcg-component-checkbox').classList.contains('dcg-checked')
      oldOptions.yAxis = settings.querySelector('.dcg-y-axis-title .dcg-component-checkbox').classList.contains('dcg-checked')
      oldOptions.xMin = +settings.querySelector('.dcg-x-axis-options [data-dcg-limit="xmin"]').innerText.replace('\u2212', '-')
      oldOptions.xMax = +settings.querySelector('.dcg-x-axis-options [data-dcg-limit="xmax"]').innerText.replace('\u2212', '-')
      oldOptions.yMin = +settings.querySelector('.dcg-y-axis-options [data-dcg-limit="ymin"]').innerText.replace('\u2212', '-')
      oldOptions.yMax = +settings.querySelector('.dcg-y-axis-options [data-dcg-limit="ymax"]').innerText.replace('\u2212', '-')

      if (oldOptions.grid) {
        if (settings.querySelector('.dcg-grid-settings .dcg-action-cartesian.dcg-active')) {
          oldOptions.minorGridLines = settings.querySelector('.dcg-top-checkboxes-container .dcg-component-checkbox:nth-child(2)').classList.contains('dcg-checked')
          oldOptions.grid = 'cartesian'
        }
        if (settings.querySelector('.dcg-grid-settings .dcg-action-polar.dcg-active'))
          oldOptions.grid = 'polar'
      }
      if (oldOptions.arrows) {
        if (settings.querySelector('.dcg-grid-settings [dcg-arrows="BOTH"].dcg-active'))
          oldOptions.arrows = 'both'
        if (settings.querySelector('.dcg-grid-settings [dcg-arrows="POSITIVE"].dcg-active'))
          oldOptions.arrows = 'positive'
      }
      return oldOptions
    })

    console.log(oldOptions)

    // Set new options
    if (options.grid != null && (!!options.grid !== !!oldOptions.grid))
      await (await page.$$('.dcg-settings-container .dcg-grid-settings .dcg-component-checkbox'))[0].click()
    if (options.grid)
      await page.click(`.dcg-settings-container .dcg-grid-settings .dcg-action-${options.grid}`)
    if (options.arrows != null && (!!options.arrows !== !!oldOptions.arrows))
      await (await page.$$('.dcg-settings-container .dcg-grid-settings .dcg-component-checkbox'))[1].click()
    if (options.arrows)
      await page.click(`.dcg-settings-container .dcg-grid-settings [dcg-arrows="${options.arrows.toUpperCase()}"]`)
    if (options.axisNumbers != null && (!!options.axisNumbers !== !!oldOptions.axisNumbers))
      await page.click('.dcg-settings-container .dcg-top-checkboxes-container .dcg-component-checkbox:nth-child(1)')
    if (options.grid === 'cartesian' && (!!options.minorGridLines !== !!oldOptions.minorGridLines))
      await page.click('.dcg-settings-container .dcg-top-checkboxes-container .dcg-component-checkbox:nth-child(2)')
    if (options.xAxis != null && (!!options.xAxis !== !!oldOptions.xAxis))
      await page.click('.dcg-settings-container .dcg-y-axis-title .dcg-component-checkbox')
    if (options.yAxis != null && (!!options.yAxis !== !!oldOptions.yAxis))
      await page.click('.dcg-settings-container .dcg-y-axis-title .dcg-component-checkbox')
    if (options.xAxis && options.xLabel)
      await setInput(page, await page.$('.dcg-settings-container .dcg-x-axis-label'), options.xLabel)
    if (options.yAxis && options.yLabel)
      await setInput(page, await page.$('.dcg-settings-container .dcg-y-axis-label'), options.yLabel)
    if (options.x != null) {
      const oldX = (oldOptions.xMin + oldOptions.xMax) / 2
      const diff = options.x - oldX
      const xMin = (oldOptions.xMin + diff).toFixed(5)
      const xMax = (oldOptions.xMax + diff).toFixed(5)
      await setInput(page, await page.$('.dcg-settings-container .dcg-x-axis-options [data-dcg-limit="xmin"]'), xMin)
      await setInput(page, await page.$('.dcg-settings-container .dcg-x-axis-options [data-dcg-limit="xmax"]'), xMax)
    }
    if (options.y != null) {
      const oldY = (oldOptions.yMin + oldOptions.yMax) / 2
      const diff = options.y - oldY
      const yMin = (oldOptions.yMin + diff).toFixed(5)
      const yMax = (oldOptions.yMax + diff).toFixed(5)
      await setInput(page, await page.$('.dcg-settings-container .dcg-y-axis-options [data-dcg-limit="ymin"]'), yMin)
      await setInput(page, await page.$('.dcg-settings-container .dcg-y-axis-options [data-dcg-limit="ymax"]'), yMax)
    }

    // Get new graph
    await page.click('.dcg-action-share')
    await page.waitForSelector('.dcg-permalink')
    dispGraphId = await page.evaluate(() => {
      const link = document.querySelector('.dcg-permalink').value
      return link.split('/').pop().slice(0, 10)
    })

    page.once('dialog', dialog => {
      dialog.accept()
    })
  }

  // Go to new graph
  if (options.width && options.height) {
    await page.setViewport({
      height: options.height,
      width: options.width
    })
  }
  await page.goto(`https://www.desmos.com/calculator/${dispGraphId}?embed`)
  await page.waitForSelector('.dcg-loading-div-container[style="display: none;"]')
  await page.evaluate(() => {
    const branding = document.querySelector('.dcg-graphpaper-branding')
    if (branding) branding.remove()
  })
  // TODO: make it work
  // if (options.zoom) {
  //   const body = await page.$('body')
  //   const boundingBox = await body.boundingBox()
  //   await page.mouse.move(
  //     boundingBox.x + boundingBox.width / 2,
  //     boundingBox.y + boundingBox.height / 2
  //   )
  //   await page.mouse.wheel({ deltaY: options.zoom * 100 })
  //   await sleep(200)
  // }
  const screenshot = await page.screenshot({
    encoding: 'binary'
  })
  await browser.close()
  return screenshot
}

function desmosSnapshotWrapper () {
  return desmosSnapshot.apply(this, arguments).catch(err => {
    if (!err.message.includes('Page crashed!')) throw err
  })
}

module.exports = desmosSnapshotWrapper
